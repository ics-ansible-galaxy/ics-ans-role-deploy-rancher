# ics-ans-role-deploy-rancher

Ansible role to install deploy-rancher.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-deploy-rancher
```

## License

BSD 2-clause
